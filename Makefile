# define the compiler to use
CC = gcc
CFLAGS = -Wall -g -pthread
INCLUDES = -I includes
CPPFLAGS = $(INCLUDES)

# define library paths in addition to /usr/lib
LFLAGS =
LIBS =

# build directory
BUILD=build
SRCDIR=src
BIN=$(BUILD)/bin
OBJECTS=$(BUILD)/objects

# output dir
OUTPUT=output

# define the source files
SRCS =  $(wildcard $(SRCDIR)/*.c)
CLIENT_SRC="./yash-client.c"

# define the object files
OBJS := $(patsubst $(SRCDIR)/%.c, $(OBJECTS)/%.o, $(SRCS) )

# define the executable file
MAIN := ${BIN}/yashd
CLIENT :=${BIN}/yash

# BAKING RECIPES

.PHONY: depend clean install

all: $(MAIN) $(CLIENT)
	@echo  `basename $(MAIN)` compiled OK

$(MAIN): $(OBJS)
	mkdir -p ${BIN} ${OUTPUT}
	$(CC) $(CFLAGS) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)
	cp ${MAIN} .

$(CLIENT):
	$(CC) $(CFLAGS) -o $(CLIENT) ${CLIENT_SRC} $(LFLAGS) $(LIBS)
	cp ${CLIENT} .

# Compile all the .c into .o's
$(OBJS) : $(OBJECTS)/%.o : $(SRCDIR)/%.c $(SRCDIR)/%.h
	mkdir -p ${OBJECTS}
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@

clean:
	@echo cleaning
	$(RM)  */*.o  $(OBJECTS)/* ${OUTPUT}/*  *~ $(MAIN) ${CLIENT} yash yashd

install:
	echo 'Installing'
	cp $(MAIN) ./

depend: $(SRCS_CC)
	makedepend $(INCLUDES) $^

# DO NOT DELETE
