#ifndef YASH_MAIN_H
#define YASH_MAIN_H

#include <unistd.h>

#define MAX_COMMAND_SIZE    200


typedef struct _job_data_t
{
    int sockD;
    int pipeFD[2];
    int workerPid;
    // char command[MAX_COMMAND_SIZE];
} job_data_t;

const job_data_t yashd_to_yash_process_job(job_data_t jobData);

#endif // YASH_MAIN_H
