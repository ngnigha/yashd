#include "main.h"
#include "yashd-server-thread.h"


#include <fcntl.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/file.h>
#include <sys/socket.h>
#include <netinet/in.h>


void yashd_sigterm_handler(int sig)
{
    if( lockf(lockFileFD,F_ULOCK, 0) == -1)
    {
        perror("Failed to unlock lock file!");
        exit(1);
    }

    close(lockFileFD);
    exit(0);
}

void yashd_sigint_handler(int sig)
{
    int fd = open(LOCK_FILE, O_RDONLY);
    if( lockf(fd,F_ULOCK, 0) == -1)
    {
        perror("Failed to unlock lock file!");
        exit(1);
    }

    exit(0);
}

void yashd_sigpipe_handler(int sig)
{
}

void yashd_signal_handler()
{
    if ( signal(SIGTERM, yashd_sigterm_handler) < 0 )
    {
        perror("Signal SIGTERM");
        exit(1);
    }

    if( signal(SIGINT, yashd_sigint_handler) < 0 )
    {
        perror("Signal SIGTERM");
        exit(1);
    }

    if( signal(SIGPIPE, yashd_sigpipe_handler) < 0 )
    {
        perror("Signal SIGTERM");
        exit(1);
    }
}

void yashd_make_daemon(void)
{
    // Close all file descriptors
    {
        int fd;
        for (fd = 0; fd < getdtablesize(); fd++)
        {
            close(fd);
        }
    }

    // Detach from controlling terminal
    {
        int pgid;
        if((pgid=setsid()) == -1)
        {
            perror("Failed to create new session!");
            exit(1);
        }
    }

    // Move to a safe directory
    {
        chdir(SAFE_DIR);
    }

    // Redirect std's
    {
        errno = 0;
        int nullFD = open("/dev/null", O_RDWR);
        int logFD = open(ERROR_LOG, O_RDWR|O_CREAT|O_SYNC|O_APPEND, 0666);

        if(nullFD == -1)
        {
            perror("Failed to open /dev/null!");
            exit(1);
        }

        if(logFD == -1)
        {
            perror("Failed to open the log file!");
            exit(1);
        }

        if( dup2(nullFD, STDIN_FILENO ) == -1 ||
            dup2(logFD, STDOUT_FILENO) == -1 ||
            dup2(logFD,  STDERR_FILENO) == -1   )
        {
            perror("Failed at yashd IO redirection!");
            exit(0);
        }

        close(nullFD);
    }

    // Read content of Lock file and kill process that owns it
    {
        int lockFD;
        if( (lockFD = open(LOCK_FILE, O_RDWR|O_CREAT|O_SYNC, 0666)) == -1 )
        {
            perror("Failed to open the Lock file!");
            exit(1);
        }

        // Read owner pid
        char ownerPid[LOCK_OWNER_PID_SIZE];
        memset(ownerPid, 0, LOCK_OWNER_PID_SIZE);
        size_t bytes = read(lockFD, ownerPid, LOCK_OWNER_PID_SIZE);

        if(bytes < 0)
        {
            perror("Reading the lock file owner pid failed!");
            exit(1);
        }
        else if(bytes > 0)
        {
            // A yashd daemon is running currently. Kill it
            int oPid = atoi(ownerPid);
            if(kill(oPid, SIGTERM) == -1 && errno != ESRCH)
            {
                perror("Failed to kill lock file owner!");
                exit(-1);
            }

            // Clear only if the file had valid bytes
            close(lockFD);
            lockFD = open(LOCK_FILE, O_WRONLY|O_CREAT|O_SYNC, 0666);
        }

        lockFileFD = lockFD;

        // Lock the file here
        if(lockf(lockFD, F_LOCK, 0) == -1)
        {
            perror("File is already locked!");
            exit(1);
        }

        sprintf(ownerPid, "%d", getpid());
        write(lockFD, ownerPid, strlen(ownerPid));
    }

    // Catch signal
    yashd_signal_handler();
}

void reusePort(int sockfd)
{
    int one=1;

    if ( setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,(char *) &one,sizeof(one)) == -1 )
    {
        printf("error in setsockopt,SO_REUSEPORT \n");
        exit(-1);
    }
}

void yashd_handle_requests(void)
{
    // Create socket
    int sockD = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sockD == -1)
    {
        perror("Failed to create a communication socket!");
        exit(1);
    }

    // Socket binding
    struct sockaddr_in servAddr, cliAddr;

    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = INADDR_ANY;
    servAddr.sin_port = htons(PORTNO);

    reusePort(sockD);
    if(bind(sockD, (struct sockaddr*) &servAddr, sizeof(servAddr)) == -1)
    {
        perror("Failed to bind port to socket");
        close(sockD);
        exit(1);
    }

    listen(sockD, 10);

    socklen_t cliLen = sizeof(cliAddr);

    while(1)
    {
        int cliSD = accept(sockD, (struct sockaddr*) &cliAddr, &cliLen);
        yashd_serve_connection(cliSD, cliAddr);
    }
}

int main(int argc, char ** argv)
{
    int pid = fork();

    if(pid == -1)
    {
        perror("Failed to fork daemon process!");
        exit(1);
    }
    else if(pid == 0)
    {
        yashd_make_daemon();
        yashd_handle_requests();
        exit(0);
    }

    return 0;
}
