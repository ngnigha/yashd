#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>


#include "yash-pcb.h"
#include "yash-main.h"
#include "yash-processing.h"
#include "yash-signals.h"
#include "yash-job-control.h"

extern pcb_t processController = {0, NULL, NULL};

int read_command_input( char *command )
{
    size_t sz = MAX_LINE_SIZE;
    size_t bytes = getline( &command, &sz, stdin);
    return bytes;
}

int process_command( pcb_t *pcb )
{
    char * tokens[MAX_LINE_SIZE];
    char *cmd = strdup(pcb->jobList->cmd);

    // Tokenize input
    const char delim[] = " ";
    char *tok = strtok( cmd, delim);

    char **iter = tokens;
    while( tok != NULL )
    {
        char *p = strchr(tok, '\n');
        if(p)
        {
            *p = 0;
        }

        *iter = tok;
        tok = strtok(NULL, delim);
        iter++;
    }
    *iter = tok;

    execute_job( pcb, tokens);
    free(cmd);
    return 0;
}

const job_data_t yashd_to_yash_process_job(job_data_t jobData)
{
    errno = 0;
    jobData.workerPid = -1;

    if(pipe(jobData.pipeFD) == -1)
    {
        perror("Failed to create pipes");
        exit(1);
    }

    if((jobData.workerPid = fork()) == -1)
    {
        perror("Failed to fork new process");
    }
    else if(jobData.workerPid == 0)
    {
        job_type_t jobType;
        char cmd[MAX_COMMAND_SIZE];

        dup2(jobData.sockD, STDOUT_FILENO);
        dup2(jobData.sockD, STDERR_FILENO);
        dup2(jobData.pipeFD[READ], STDIN_FILENO);

        close(jobData.sockD);
        close(jobData.pipeFD[WRITE]);
        close(jobData.pipeFD[READ]);

        signal(SIGINT,  yash_sigint_handler);
        signal(SIGTSTP, yash_sigstop_handler);
        signal(SIGTERM, yash_sigterm_handler);
        signal(SIGCHLD, yash_sigchld_handler);

        write(STDOUT_FILENO, "\n# ", 3);

        while(TRUE)
        {
            memset(cmd, 0, MAX_COMMAND_SIZE);

            // Read from the pipe
            if(read_command_input(cmd) < 0)
            {
                perror("Failed to read from the pipe");
                exit(1);
            }

            // Determine job type and execute
            if((jobType = yash_get_job_type(cmd)) == FORKJOB )
            {
                jobDescription_t *job = newJob(-1, cmd);
                add_job_to_pcb(&processController, job);
                process_command( &processController );
                write(STDOUT_FILENO, "\n# ", 3);
            }
            else
            {
                yash_process_control_job(jobType);
            }
        }

        exit(0);
    }

    close(jobData.pipeFD[READ]);
    return jobData;
}
