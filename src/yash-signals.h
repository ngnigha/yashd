#ifndef YASH_SIGNALS_H
#define YASH_SIGNALS_H

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include "yash-pcb.h"

void yash_sigint_handler(int signum);
void yash_sigterm_handler(int signum);
void yash_sigstop_handler(int signum);
void yash_sigchld_handler(int signum);

void yash_clean_exit(void);
void yash_send_signal(jobDescription_t *job, int signum);

#endif // YASH_SIGNALS
