#include "yash-job-control.h"
#include "yash-pcb.h"
#include "yash-signals.h"
#include "yash-processing.h"

#include <unistd.h>
#include <string.h>

#define MAX_CONTROL_SIZE 512

extern pcb_t processController;
static char controlMessage[MAX_CONTROL_SIZE];

static void yash_jobs( jobDescription_t *jobs )
{
    if( jobs == NULL )
        return;

    yash_jobs(jobs->next);

    char status[16];

    if( jobs->status == DONE )
    {
        sprintf(status, "Done" );
    }
    else if( jobs->status == RUNNING )
    {
        sprintf(status, "Running" );
    }
    else if( jobs->status == STOPPED )
    {
        sprintf(status, "Stopped" );
    }
    else if( jobs->status == READY )
    {
        sprintf(status, "Ready" );
    }

    bzero(controlMessage, MAX_CONTROL_SIZE);
    sprintf(controlMessage, "[%d] %c %s \t%s\n", jobs->id,
            (jobs->isBackground==TRUE || jobs->status==STOPPED ? '+':'-'), status, jobs->cmd);
    write(STDOUT_FILENO, controlMessage, strlen(controlMessage));
}

static void yash_foreground(void)
{
    jobDescription_t *latest = NULL;
    jobDescription_t *job  = processController.jobList;

    while(job)
    {
        if( (job->status == STOPPED || job->isBackground) && (!latest || latest->lastUpdate < job->lastUpdate) )
        {
            latest = job;
        }
        job = job->next;
    }

    if(latest)
    {
        yash_update_to_background(latest, FALSE, 0);
        latest->status = RUNNING;
        processController.fgJob = latest;
        sprintf(controlMessage, "[%d] %c %s \t%s\n", latest->id,
                (latest->isBackground==TRUE || latest->status==STOPPED ? '+':'-'), "Running", latest->cmd);
        write(STDOUT_FILENO, controlMessage, strlen(controlMessage));
        yash_send_signal(latest, SIGCONT);
    }
}

static void yash_background(void)
{
    jobDescription_t *latest = NULL;
    jobDescription_t *job  = processController.jobList;

    while(job)
    {
        if(job->status == STOPPED && (!latest || latest->lastUpdate < job->lastUpdate) )
        {
            latest = job;
        }
        job = job->next;
    }

    if(latest)
    {
        yash_update_to_background(latest, TRUE, ++processController.LastUpdate);
        latest->status = RUNNING;
        bzero(controlMessage, MAX_CONTROL_SIZE);
        sprintf(controlMessage, "[%d] %c %s \t%s\n", latest->id,
                (latest->isBackground==TRUE || latest->status==STOPPED ? '+':'-'), "Running", latest->cmd);
        write(STDOUT_FILENO, controlMessage, strlen(controlMessage));
        yash_send_signal(latest, SIGCONT);
    }
}

static void yash_newline(void)
{
    jobDescription_t *cdt = NULL;
    jobDescription_t *job  = processController.jobList;

    while( job != NULL )
    {
        if( job->status == DONE )
        {
            bzero(controlMessage, MAX_CONTROL_SIZE);
            sprintf(controlMessage, "[%d] %c %s \t%s\n", job->id,
                    (job->isBackground==TRUE || job->status==STOPPED ? '+':'-'), "Done", job->cmd);
            write(STDOUT_FILENO, controlMessage, strlen(controlMessage));
            cdt = job;
        }
        job = job->next;
        delete_job(&processController, cdt);
    }
}

job_type_t yash_get_job_type(const char * cmd)
{
    if(cmd && cmd[0] == '\n')
    {
        return NEWLINE;
    }
    else if(strstr(cmd, "jobs"))
    {
        return JOBS;
    }
    else if(strstr(cmd, "fg"))
    {
        return FOREGROUND;
    }
    else if(strstr(cmd, "bg"))
    {
        return BACKGROUND;
    }

    return FORKJOB;
}

void yash_process_control_job(const job_type_t type)
{
    switch (type)
    {
    case JOBS:
    {
        yash_jobs(processController.jobList);
        break;
    }
    case NEWLINE:
    {
        yash_newline();
        break;
    }
    case FOREGROUND:
    {
        yash_foreground();
        return;
    }
    case BACKGROUND:
    {
        yash_background();
        break;
    }
    default:
    {
        break;
    }
    }
    write(STDOUT_FILENO, "\n# ", 3);
}
