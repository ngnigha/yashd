#include "yash-main.h"
#include "yashd-server-thread.h"

#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/wait.h>


pthread_mutex_t logMutex = PTHREAD_MUTEX_INITIALIZER;

static  int yashd_read_command(const int sockD, char *cmd)
{
    const char *msg = "Command length exceeded the 200 bytes";

    int bytes = read(sockD, cmd, MAX_COMMAND_SIZE);
    if( bytes > MAX_COMMAND_SIZE)
    {
        write(sockD, msg, strlen(msg));
        return -2;
    }

    return bytes;
}

static void yashd_log_command(const thread_data_t *tData, const char* data)
{
    char buf[32];
    char logData[256];
    time_t rawtime;
    struct tm * curtime;

    time(&rawtime);
    curtime = localtime(&rawtime);

    strftime(buf, sizeof(buf), "%b %d %H:%M:%S", curtime);
    sprintf(logData, "%s yashd [%s:%d]: %s", buf, inet_ntoa(tData->cliAddr.sin_addr), tData->cliAddr.sin_port, data);

    if(pthread_mutex_lock(&logMutex) == -1)
    {
        perror("Failed to lock mutex");
        exit(1);
    }
    {
        int fd = open(LOG_FILE, O_RDWR|O_CREAT|O_APPEND, 0666);
        if( fd == -1)
        {
            perror("Failed to open log file");
        }
        if(write(fd, logData, strlen(logData)) == -1)
        {
            perror("Write failure");
        }
        close(fd);
    }
    if(pthread_mutex_unlock(&logMutex) == -1)
    {
        perror("Failed to unlock mutex");
        exit(1);
    }
}

static void yashd_forward_command(thread_data_t *tData, char *command)
{
    int stat;
    errno = 0;
    char *p = NULL;

    if((p = strstr(command, CMD_COMMAND)))
    {
        // This is a command
        p = p + strlen(CMD_COMMAND);
        write(tData->writePipeFD, p, strlen(p));
    }
    else if((p = strstr(command, CTL_COMMAND)))
    {
        // This is a control
        p = p + strlen(CTL_COMMAND);

        char *sig;
        if((sig = strchr(p, 'c')))
        {
            kill(tData->workerPid, SIGINT);
        }
        else if((sig = strchr(p, 'd')))
        {
            kill(tData->workerPid, SIGTERM);
            close(tData->sockD);
            close(tData->writePipeFD);
            waitpid(tData->workerPid, &stat, WCONTINUED|WUNTRACED);
            pthread_exit(0);
        }
        else if((sig = strchr(p, 'z')))
        {
            kill(tData->workerPid, SIGTSTP);
        }
    }
    else
    {
        perror("invalid command");
    }
}

static void* yashd_thread_handler(void* tData)
{
    int stat;
    int bytes;
    job_data_t jobData;
    char buf[MAX_BUFFER_SIZE];

    // Copy tData and detach
    thread_data_t data = *((thread_data_t*) tData);
    free(tData);

    jobData.sockD = data.sockD;
    jobData = yashd_to_yash_process_job(jobData);
    data.workerPid = jobData.workerPid;
    data.writePipeFD = jobData.pipeFD[1];

    while(1)
    {
        // Clear command buffer
        bzero(buf, MAX_BUFFER_SIZE);

        bytes = yashd_read_command(data.sockD, buf);
        if(bytes == -2)
        {
            continue;
        }
        else if(bytes == -1)
        {
            perror("Read of command failed");
            break;
        }
        else if(bytes == 0)
        {
            perror("Read of command failed");
            fflush(fdopen(data.sockD, "rw"));
            break;
        }

        // Log the current job
        yashd_log_command(&data, buf);

        // Handle the command
        yashd_forward_command(&data, buf);
    }

    close(data.sockD);
    close(data.writePipeFD);
    waitpid(data.workerPid, &stat, WCONTINUED|WUNTRACED);
    pthread_exit(0);
}

void yashd_serve_connection(const int sockD, const struct sockaddr_in cliAddr)
{
    // Create the handler thread
    pthread_t tid;
    thread_data_t *data = (thread_data_t*) malloc(sizeof(thread_data_t));
    data->sockD = sockD;
    data->cliAddr = cliAddr;
    data->workerPid = -1;

    if( pthread_create(&tid, NULL, yashd_thread_handler, (void*)data) == -1)
    {
        perror("Failed to create a new Thread\n");
        exit(1);
    }
}

