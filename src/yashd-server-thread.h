#ifndef YASHD_SERVER_THREAD_H
#define YASHD_SERVER_THREAD_H

#include <pthread.h>
#include <netinet/in.h>

#define MAX_BUFFER_SIZE     512
#define MAX_COMMAND_SIZE    200
#define RECEIVE_PROMPT      "\n# "
#define LOG_FILE            "/tmp/yashd.log"

#define CMD_COMMAND         "CMD "
#define CTL_COMMAND         "CTL "


typedef struct _thread_data_t
{
    int sockD;
    int logFD;
    int writePipeFD;
    pid_t workerPid;
    struct sockaddr_in cliAddr;
} thread_data_t;


void yashd_serve_connection(const int sockD, const struct sockaddr_in cliAddr);

// void yashd_serve_connection(const int sockD);





#endif // YASHD_SERVER_THREAD_H
