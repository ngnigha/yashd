#ifndef YASHD_MAIN_H
#define YASHD_MAIN_H

#include <errno.h>

#define SAFE_DIR    "/tmp"
#define LOCK_FILE   SAFE_DIR"/yashd.pid"
#define ERROR_LOG   SAFE_DIR"/error.log"

int lockFileFD;

// Socket info
#define PORTNO      3826

// buffers' sizes
#define LOCK_OWNER_PID_SIZE     8

#endif // YASHD_MAIN_H
