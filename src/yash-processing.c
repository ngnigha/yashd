#include "yash-processing.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <errno.h>
#include <sys/stat.h>


static int perform_file_redirection(taskDescription_t *task)
{
    if(task->stdinFileRedirect == TRUE)
    {
        // if filename does not exist, abort
        int fd = open( task->stdinFile, O_RDONLY );
        if( fd == -1)
        {
            perror("Error! ");
            exit(1);
        }

        if( dup2(fd, STDIN_FILENO) == -1)
        {
            perror("Error! ");
            exit(1);
        }

        close(fd);
    }

    if(task->stdoutFileRedirect == TRUE)
    {
        // if filename does not exist, create one
        int fd = open( task->stdoutFile, O_WRONLY|O_CREAT, 0666 );

        if( fd == -1)
        {
            perror("Error! ");
            exit(1);
        }

        if( dup2(fd, STDOUT_FILENO) == -1)
        {
            perror("Error! ");
            exit(1);
        }

        close(fd);
    }

    if(task->stderrFileRedirect == TRUE)
    {
        // if filename does not exist, create one
        int fd = open( task->stderrFile, O_WRONLY|O_CREAT );

        if( fd == -1)
        {
            perror("Error! ");
            exit(1);
        }

        if( dup2(fd, STDERR_FILENO) == -1)
        {
            perror("Error! ");
            exit(1);
        }

        close(fd);
    }

    return 0;
}

static int perform_pipe_renaming(taskDescription_t *prev, taskDescription_t *task)
{

    if( task->redirectStdinToPipe)
    {
        if( prev )
        {
            if( dup2(prev->pipes[READ], STDIN_FILENO) )
            {
                perror("Error: ");
                exit(1);
            }
        }
    }

    if( task->redirectStdoutToPipe )
    {
        if( prev == NULL )
        {
            close(task->pipes[READ]);
        }

        if( dup2(task->pipes[WRITE], STDOUT_FILENO) == -1 )
        {
            perror("Error: ");
            exit(1);
        }
        close(task->pipes[WRITE]);
    }

    if( prev && prev->redirectStdoutToPipe )
    {
        close(prev->pipes[READ]);
        close(prev->pipes[WRITE]);
    }

    return 0;
}

static pid_t fork_jobs( pcb_t *pcb )
{
    jobDescription_t *job = pcb->jobList;
    taskDescription_t *task = job->taskList;
    taskDescription_t *prev = NULL;

    job->status = RUNNING;
    while( task != NULL )
    {
        bool_t closePipes = FALSE;
        if( task->redirectStdoutToPipe )
        {
            pipe(task->pipes);
            closePipes = TRUE;
        }

        pid_t pid = fork();

        if( pid < 0 )
        {
            perror("Failure to fork new process: ");
            exit(1);
        }
        else if( pid == 0 )
        {
            // Pipes redirection
            perform_pipe_renaming(prev, task);

            // File redirection
            perform_file_redirection(task);

            if( strlen(task->cmdAndArgs[0]) == 0 )
            {
            }
            else if( execvp( task->cmdAndArgs[0], task->cmdAndArgs) == -1 )
            {
                perror("Error: execvp failed!");
                exit(1);
            }
            exit(0);
        }
        else
        {
            // Main process
            task->pid = pid;
            if( prev && prev->redirectStdoutToPipe )
            {
                close(prev->pipes[READ]);
                close(prev->pipes[WRITE]);
            }
        }

        prev = task;
        task = task->next;
    }

    if(!job->isBackground)
    {
        pcb->fgJob = job;
        yash_wait_job(job);
    }

    return 0;
}

static pid_t prepare_tasks( taskDescription_t *task )
{
    char **commandArgs = task->cmdAndArgs;
    char **iter = commandArgs;
    redirect_mode_t rMode = NONE;

    do
    {
        char *str = *iter;

        if( str == NULL )
        {
            break;
        }
        else if( strcmp(str, INPUT_REDIRECTION) == 0 )
        {
            *iter = 0;
            task->stdinFileRedirect = TRUE;

            iter++;
            if( *iter == NULL )
            {
                exit(1);
            }
            task->stdinFile = strdup( *iter );
        }
        else if( strcmp(str, OUTPUT_REDIRECTION) == 0 )
        {
            *iter = 0;
            task->stdoutFileRedirect = TRUE;

            iter++;
            if( *iter == NULL )
            {
                exit(1);
            }
            task->stdoutFile = strdup( *iter );
        }
        else
        {
        }

        iter++;
    } while(1);

    return 0;
}

pid_t yash_wait_job(jobDescription_t *job)
{
    if(job == NULL)
    {
        return -1;
    }

    taskDescription_t *task = job->taskList;
    pid_t ret = task->pid;

    while( task )
    {
        int exitStatus;
        waitpid(task->pid, &exitStatus, WUNTRACED | WCONTINUED);
        task = task->next;
    }

    return ret;
}

pid_t execute_job( pcb_t *pcb, char **commandArgs )
{
    char **iter = commandArgs;
    char **taskBegin = commandArgs;
    bool_t pipeMode = FALSE;

    do
    {
        char *str = *iter;

        if( str == NULL )
        {
            taskDescription_t *sTask = newTask(0, taskBegin);
            sTask->redirectStdinToPipe  = pipeMode ? TRUE: FALSE;

            prepare_tasks(sTask);
            add_task_to_job(pcb->jobList, sTask);
            break;
        }
        else if( strcmp(str, BACKGROUND_JOB ) == 0 )
        {
            if( *(iter + 1) == NULL )
            {
                *iter = NULL;
                yash_update_to_background(pcb->jobList, TRUE, ++pcb->LastUpdate);
            }
        }
        else if( strcmp(str, PIPE_OPERATION) == 0 )
        {
            *iter = NULL;
            taskDescription_t *pTask = newTask(0, taskBegin);
            pTask->redirectStdoutToPipe = TRUE;
            pTask->redirectStdinToPipe  = pipeMode ? TRUE: FALSE;

            prepare_tasks(pTask);
            add_task_to_job(pcb->jobList, pTask);

            taskBegin = iter + 1;
            pipeMode = TRUE;
        }
        else
        {
        }

        iter++;
    } while(1);

    fork_jobs(pcb);
    return 0;
}
