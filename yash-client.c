#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>


#define PORTNO              3826
#define CMD_STRING          "CMD \0"
#define MAX_READ_SIZE       512
#define MAX_INPUT_SIZE      256
#define RECEIVE_PROMPT      "\n# "
#define QUIT_SESSION_MSG    "quit"


int socketDescriptor;

static void * yash_read_and_display(void *sockID);

static void yash_clean_exit(int exitCode)
{
    exit(exitCode);
}

static void yash_sigtstp_handler(int sig)
{
    const char ctrlCmd[16] = "CTL z\n";
    write(socketDescriptor, ctrlCmd, strlen(ctrlCmd));
}

static void yash_sigint_handler(int sig)
{
    const char ctrlCmd[16] = "CTL c\n";
    write(socketDescriptor, ctrlCmd, strlen(ctrlCmd));
}

static void yash_sigusr1_handler(int sig)
{
    const char ctrlCmd[16] = "CTL d\n";
    write(socketDescriptor, ctrlCmd, strlen(ctrlCmd));
    yash_clean_exit(0);
}

static void yash_signal_handler()
{
    if ( signal(SIGTSTP, yash_sigtstp_handler) < 0 )
    {
        perror("Signal SIGTSTP");
        yash_clean_exit(1);
    }

    if( signal(SIGINT, yash_sigint_handler) < 0 )
    {
        perror("Signal SIGINT");
        yash_clean_exit(1);
    }

    if( signal(SIGUSR1, yash_sigusr1_handler) < 0 )
    {
        perror("Signal SIGUSR1");
        yash_clean_exit(1);
    }
}

static int yash_read_input(char *cmd)
{
    strcpy(cmd, CMD_STRING);
    int maxCount = MAX_INPUT_SIZE;
    char *tp = cmd + strlen(CMD_STRING);
    ssize_t bytes = getline(&tp, &maxCount, stdin);

    if(bytes == -1 || strstr(tp, QUIT_SESSION_MSG))
    {
        // Exit this session
        kill(getpid(), SIGUSR1);
    }

    return strlen(cmd);
}

static void * yash_read_and_display(void *sockID)
{
    int bytes;
    char buf[MAX_READ_SIZE+1];
    const int sockD = (int)sockID;

    pthread_detach(pthread_self());
    fflush(fdopen(STDOUT_FILENO, "rw"));

    while(1)
    {
        memset(buf, 0, MAX_READ_SIZE+1);
        bytes = read(sockD, buf, MAX_READ_SIZE);

        if(bytes < 0)
        {
            perror("Failed to read form server;");
            yash_clean_exit(1);
        }
        else if(bytes == 0)
        {
            perror("Server read no bytes");
            break;
        }

        printf("%s", buf);
    }

    pthread_exit(1);
}

int main(int argc, char **argv )
{
    if(argc != 2)
    {
        printf("Error: Missing the ip address!\n");
        exit(1);
    }

    struct hostent *servHostent;
    struct sockaddr_in servAddr;

    int sockD = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sockD == -1)
    {
        perror("Failed to create a socket!");
        exit(1);
    }

    servHostent = gethostbyname(argv[1]);
    if(servHostent == NULL)
    {
        perror("Failed to get host name converted!");
        exit(1);
    }

    bzero((char *) &servAddr, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    bcopy ( servHostent->h_addr, &(servAddr.sin_addr.s_addr), servHostent->h_length);
    servAddr.sin_port = htons(PORTNO);

    if(connect(sockD, (struct  sockaddr *) &servAddr, sizeof(servAddr)) != 0)
    {
        perror("Failed to connect to server host!");
        exit(1);
    }

    setvbuf(stdout, NULL, _IONBF, 0);

    socketDescriptor = sockD;
    yash_signal_handler();

    // Create a socket reader thread
    pthread_t socketReader;
    pthread_create(&socketReader, NULL, yash_read_and_display, (void*)sockD);

    char command[MAX_INPUT_SIZE];
    while(1)
    {
        // take user input and send over to server
        int bytes = yash_read_input(command);
        bytes = write(sockD, command, bytes);
    }

    return 0;
}
